#!/usr/bin/python
import sys, time
import os
import os.path
import OpenGL
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GL import framebufferobjects
import texreader
import shaderreader


BASEDIR = os.path.abspath(os.path.dirname(__file__))


def load_textures(texturePath):
	res = []

	for name in os.listdir(texturePath):
		if not name.endswith(".ppm"):
			continue

		fullpath = os.path.join(texturePath, name)
		res.append(
				(name, texreader.textureFromFile(fullpath))
			)

	res.sort()

	return res


def scale_integer(windowW, windowH, inputW, inputH):
	"""
	Calculates the image as the largest integer multiple of raw frame size.
	"""
	scaleX = windowW / inputW
	scaleY = windowH / inputH

	if scaleX > 0 and scaleY > 0:
		scale = min(scaleX, scaleY)
		finalW = scale * inputW
		finalH = scale * inputH
	else:
		if scaleX == 0:
			finalW = windowW
		else:
			finalW = inputW

		if scaleY == 0:
			finalH = windowH
		else:
			finalH = inputH

	return finalW, finalH


def scale_aspect(windowW, windowH, inputW, inputH):
	"""
	Calculates the image as the largest multiple of the raw frame size.
	"""
	scale = min(
			float(windowW) / float(inputW),
			float(windowH) / float(inputH),
		)

	return inputW * scale, inputH * scale


def scale_max(windowW, windowH, inputW, inputH):
	"""
	Stretch the image to the largest possible size, regardless of aspect.
	"""
	return windowW, windowH


SCALE_METHODS = [
		("Integer", scale_integer),
		("Aspect-correct", scale_aspect),
		("Maximized", scale_max),
	]


class GLUTDemo(object):

	def __init__(self, shaderFile, texturePath):
		self.start = time.clock()
		self.framecount = 0

		glutInitDisplayMode(GLUT_RGBA)

		glutInitWindowSize(640, 480)
		self.window = glutCreateWindow("XML Shader Demo - Esc to exit")
		self.windowW = None
		self.windowH = None

		glutDisplayFunc(self._draw_scene)
		glutIdleFunc(self._handle_idle)
		glutReshapeFunc(self._handle_resize)
		glutKeyboardFunc(self._handle_key)

		glClearColor(0.0, 0.0, 0.0, 0.0)
		glEnable(GL_TEXTURE_2D)

		with open(shaderFile, "r") as handle:
			self.shaderPasses = shaderreader.parse_shader(handle.read())

		self.textures = load_textures(texturePath)
		textureMenu = glutCreateMenu(self._set_texture)
		for index, (filename, _) in enumerate(self.textures):
			glutAddMenuEntry(filename, index)

		self.inputTexture = None
		self._set_texture(0)

		self.framebufferID = framebufferobjects.glGenFramebuffers(1)
		self.framebufferTexture1, self.framebufferTexture2 = \
				glGenTextures(2)

		scaleMenu = glutCreateMenu(self._set_scale_method)
		for index, (desc, _) in enumerate(SCALE_METHODS):
			glutAddMenuEntry(desc, index)

		self._set_scale_method(0)

		self.masterMenu = glutCreateMenu(lambda _: 0)
		glutAddSubMenu("Test pattern", textureMenu)
		glutAddSubMenu("Scale method", scaleMenu)
		glutAttachMenu(GLUT_RIGHT_BUTTON)

	def _set_texture(self, textureIndex):

		_, self.inputTexture = self.textures[textureIndex]

		# According to the OpenGL docs, a glutCreateMenu callback doesn't need
		# to return anything. PyOpenGL seems to think differently.
		return 0

	def _set_scale_method(self, index):
		self.scaleMethod = SCALE_METHODS[index][1]

		# According to the OpenGL docs, a glutCreateMenu callback doesn't need
		# to return anything. PyOpenGL seems to think differently.
		return 0

	def _setUniform(self, setter, program, name, *values):
		loc = glGetUniformLocation(program, name)
		if loc < 0:
			return

		setter(loc, *values)

	def _draw_texture(self, shaderPass, texture, x, y, width, height):
		if shaderPass is not None:
			glUseProgram(shaderPass.programID)

			self._setUniform(glUniform2f, shaderPass.programID,
					"rubyInputSize", texture.imageWidth, texture.imageHeight)
			self._setUniform(glUniform2f, shaderPass.programID,
					"rubyOutputSize", width, height)
			self._setUniform(glUniform2f, shaderPass.programID,
					"rubyTextureSize",
					texture.textureWidth, texture.textureHeight)
			self._setUniform(glUniform1i, shaderPass.programID,
					"rubyFrameCount", self.framecount)

			if shaderPass.filterMethod == shaderreader.ATTR_FILTER_LINEAR:
				filterID = GL_LINEAR
			else:
				filterID = GL_NEAREST

		else:
			# There's no shader pass to tell us what to do, so go with a safe
			# default.
			if shaderreader.ATTR_FILTER_DEFAULT == shaderreader.ATTR_FILTER_LINEAR:
				filterID = GL_LINEAR
			else:
				filterID = GL_NEAREST

		glBindTexture(GL_TEXTURE_2D, texture.textureID)

		glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterID)
		glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterID)

		glClear(GL_COLOR_BUFFER_BIT)

		glBegin(GL_QUADS)                   # Start drawing a 4 sided polygon

		texW = float(texture.imageWidth) / texture.textureWidth
		texH = float(texture.imageHeight) / texture.textureHeight

		glTexCoord2f(0,    0   ); glVertex(x,       y,      )
		glTexCoord2f(texW, 0   ); glVertex(x+width, y,      )
		glTexCoord2f(texW, texH); glVertex(x+width, y+height)
		glTexCoord2f(0,    texH); glVertex(x,       y+height)

		glEnd()                             # We are done with the polygon

		if shaderPass is not None:
			glUseProgram(0)

	def _draw_texture_to_fbo(self, shaderPass, fromTexture, finalW, finalH):

		# How big should the output of this pass be?
		outputW, outputH = shaderPass.calculateFramebufferSize(
				fromTexture.imageWidth, fromTexture.imageHeight,
				finalW, finalH,
			)

		# If this pass doesn't specify a size, default to the input.
		if outputW is None: outputW = fromTexture.imageWidth
		if outputH is None: outputH = fromTexture.imageWidth

		# Make a texture exactly that big.
		toTexture = texreader.Texture(outputW, outputH)

		# Configure OpenGL to render to the texture.
		framebufferobjects.glBindFramebuffer(
				framebufferobjects.GL_FRAMEBUFFER,
				self.framebufferID,
			)
		framebufferobjects.glFramebufferTexture2D(
				framebufferobjects.GL_FRAMEBUFFER,
				framebufferobjects.GL_COLOR_ATTACHMENT0,
				GL_TEXTURE_2D,
				toTexture.textureID,
				0,
			)

		# Are we all set?
		framebufferobjects.checkFramebufferStatus()

		glViewport(0, 0, int(outputW), int(outputH))
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		# A framebufferobject will always have the underlying texture's (0,0)
		# at the lower-left, so align our coordinate system to match.
		glOrtho(0, outputW, 0, outputH, -1, 1)

		self._draw_texture(shaderPass, fromTexture, 0, 0, outputW, outputH)

		# Detach all the things we set up.
		framebufferobjects.glBindFramebuffer(
				framebufferobjects.GL_FRAMEBUFFER,
				0,
			)

		return toTexture

	def _draw_scene(self):
		if None in (self.inputTexture, self.windowW, self.windowH):
			return

		finalW, finalH = self.scaleMethod(
				self.windowW, self.windowH,
				self.inputTexture.imageWidth, self.inputTexture.imageHeight,
			)

		finalX = (self.windowW - finalW) / 2
		finalY = (self.windowH - finalH) / 2

		# Will we need an implicit pass at the end of this?
		requiresImplicitPass = self.shaderPasses[-1].requiresImplicitPass()

		# Render all but the last pass
		fromTexture = self.inputTexture
		for shaderPass in self.shaderPasses[:-1]:
			fromTexture = self._draw_texture_to_fbo(
					shaderPass, fromTexture, finalW, finalH)

		# If the last pass expects to be rendered at some specific scale, we'd
		# better respect its wishes.
		if requiresImplicitPass:
			fromTexture = self._draw_texture_to_fbo(
					self.shaderPasses[-1], fromTexture, finalW, finalH)
			lastPass = None
		else:
			lastPass = self.shaderPasses[-1]

		glViewport(0, 0, self.windowW, self.windowH)
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		# After all the shader passes, the (0,0) corner of the resulting
		# texture is supposed to appear at the top-left, so let's set our
		# coordinate system appropriately.
		glOrtho(0, self.windowW, self.windowH, 0, -1, 1)

		self._draw_texture(lastPass, fromTexture,
				finalX, finalY, finalW, finalH,
			)

		glFlush()

	def _handle_key(self, key, x, y):
		if key == '\x1b': # Escape
			self._shutdown()

	def _handle_resize(self, width, height):
		self.windowW = max(width, 1)
		self.windowH = max(height, 1)

	def _handle_idle(self):
		now = time.clock()
		if now > self.start + 1:
			fps = self.framecount / (now - self.start)
			sys.stdout.write("FPS: %0.1f\r" % fps)
			sys.stdout.flush()
			self.framecount = 0
			self.start = now

		glutPostWindowRedisplay(self.window)
		self.framecount += 1

	def _shutdown(self):
		# clean up textures we've allocated.
		for _, texture in self.textures:
			texture.destroy()

		# There's no clean way to kill GLUT.
		sys.exit(0)

if __name__ == "__main__":
	argv = glutInit(sys.argv)

	if len(argv) != 2:
		print >> sys.stderr, "Usage: %s /path/to/shaderfile" % (argv[0],)
		sys.exit(1)

	shaderFile = argv[1]
	texturePath = os.path.join(BASEDIR, "test-patterns")
	demo = GLUTDemo(shaderFile, texturePath)

	glutMainLoop()
