#!/usr/bin/python
from xml.etree import ElementTree as ET
from OpenGL.GL import *
from OpenGL.GL import shaders


SCALE_METHOD_FIXED = "fixed"
SCALE_METHOD_INPUT_SCALE = "input"
SCALE_METHOD_OUTPUT_SCALE = "output"
SCALE_METHOD_NONE = None


ELEM_SHADER = "shader"
ELEM_VERTEX = "vertex"
ELEM_FRAGMENT = "fragment"

ATTR_LANGUAGE = "language"
ATTR_LANGUAGE_GLSL = "GLSL"
ATTR_FILTER = "filter"
ATTR_FILTER_NEAREST = "nearest"
ATTR_FILTER_LINEAR = "linear"
ATTR_FILTER_DEFAULT = ATTR_FILTER_LINEAR
ATTR_SIZE = "size"
ATTR_SIZE_X = "size_x"
ATTR_SIZE_Y = "size_y"
ATTR_SCALE = "scale"
ATTR_SCALE_X = "scale_x"
ATTR_SCALE_Y = "scale_y"
ATTR_OUTSCALE = "outscale"
ATTR_OUTSCALE_X = "outscale_x"
ATTR_OUTSCALE_Y = "outscale_y"


class ShaderReaderException(Exception):
	pass


class ShaderPass(object):

	def __init__(self, elements):
		parts = []
		for elem in elements:
			if elem.tag == ELEM_VERTEX:
				parts.append(
						shaders.compileShader(elem.text, GL_VERTEX_SHADER)
					)
			else:
				parts.append(
						shaders.compileShader(elem.text, GL_FRAGMENT_SHADER)
					)

		self.programID = shaders.compileProgram(*parts)

		fragmentElem = elements[-1]

		self.filterMethod = fragmentElem.get(ATTR_FILTER, ATTR_FILTER_DEFAULT)
		if self.filterMethod not in (ATTR_FILTER_NEAREST, ATTR_FILTER_LINEAR):
			raise ShaderReaderException("'filter' attribute should be "
					"'nearest' or 'linear', not %r" % (self.filterMethod,))

		self.horizontalScaleMethod = SCALE_METHOD_NONE
		self.horizontalScaleValue = None
		self.verticalScaleMethod = SCALE_METHOD_NONE
		self.verticalScaleValue = None

		self._set_scale(fragmentElem, ATTR_SIZE, int, True,
				SCALE_METHOD_FIXED)
		self._set_scale(fragmentElem, ATTR_SIZE, int, False,
				SCALE_METHOD_FIXED)
		self._set_scale(fragmentElem, ATTR_SIZE_X, int, True,
				SCALE_METHOD_FIXED)
		self._set_scale(fragmentElem, ATTR_SIZE_Y, int, False,
				SCALE_METHOD_FIXED)
		self._set_scale(fragmentElem, ATTR_SCALE, float, True,
				SCALE_METHOD_INPUT_SCALE)
		self._set_scale(fragmentElem, ATTR_SCALE, float, False,
				SCALE_METHOD_INPUT_SCALE)
		self._set_scale(fragmentElem, ATTR_SCALE_X, float, True,
				SCALE_METHOD_INPUT_SCALE)
		self._set_scale(fragmentElem, ATTR_SCALE_Y, float, False,
				SCALE_METHOD_INPUT_SCALE)
		self._set_scale(fragmentElem, ATTR_OUTSCALE, float, True,
				SCALE_METHOD_OUTPUT_SCALE)
		self._set_scale(fragmentElem, ATTR_OUTSCALE, float, False,
				SCALE_METHOD_OUTPUT_SCALE)
		self._set_scale(fragmentElem, ATTR_OUTSCALE_X, float, True,
				SCALE_METHOD_OUTPUT_SCALE)
		self._set_scale(fragmentElem, ATTR_OUTSCALE_Y, float, False,
				SCALE_METHOD_OUTPUT_SCALE)

	def _set_scale(self, elem, fieldName, parser, horiz, method):
		if fieldName not in elem.attrib:
			return

		try:
			value = parser(elem.attrib[fieldName])
		except ValueError:
			raise ShaderReaderException("Field %s should have a value "
					"compatible with %r, not %r" % (
						fieldName, parser, elem.attrib[fieldName],
					)
				)

		if horiz:
			if self.horizontalScaleValue is not None:
				raise ShaderReaderException("Can't apply attribute %s "
						"because this shader pass already sets a "
						"horizontal scaling method.")
			self.horizontalScaleMethod = method
			self.horizontalScaleValue = value

		else:
			if self.verticalScaleValue is not None:
				raise ShaderReaderException("Can't apply attribute %s "
						"because this shader pass already sets a "
						"vertical scaling method.")
			self.verticalScaleMethod = method
			self.verticalScaleValue = value

	def calculateFramebufferSize(self, inputW, inputH, finalW, finalH):
		if self.horizontalScaleMethod == SCALE_METHOD_INPUT_SCALE:
			outputW = inputW * self.horizontalScaleValue
		elif self.horizontalScaleMethod == SCALE_METHOD_OUTPUT_SCALE:
			outputW = finalW * self.horizontalScaleValue
		elif self.horizontalScaleMethod == SCALE_METHOD_FIXED:
			outputW = self.horizontalScaleValue
		else:
			outputW = None

		if self.verticalScaleMethod == SCALE_METHOD_INPUT_SCALE:
			outputH = inputH * self.verticalScaleValue
		elif self.verticalScaleMethod == SCALE_METHOD_OUTPUT_SCALE:
			outputH = finalH * self.verticalScaleValue
		elif self.verticalScaleMethod == SCALE_METHOD_FIXED:
			outputH = self.verticalScaleValue
		else:
			outputH = None

		return outputW, outputH

	def requiresImplicitPass(self):
		if self.horizontalScaleMethod != SCALE_METHOD_NONE:
			return True
		if self.verticalScaleMethod != SCALE_METHOD_NONE:
			return True
		return False


def parse_shader(data):
	root = ET.fromstring(data)

	if root.tag != ELEM_SHADER:
		raise ShaderReaderException("Root element of XML shader should be "
				"<shader/>")

	if root.get(ATTR_LANGUAGE) != ATTR_LANGUAGE_GLSL:
		raise ShaderReaderException("Currently, only GLSL shaders supported.")

	shaderPasses = []
	shaderParts = []

	for elem in root.getchildren():
		if elem.tag not in (ELEM_VERTEX, ELEM_FRAGMENT):
			continue

		if elem.tag == ELEM_VERTEX:
			if shaderParts:
				raise ShaderReaderException("Found a new <vertex/> element "
						"before the previous shader pass was complete. This "
						"shader pass is malformed.")

			shaderParts.append(elem)

			continue

		# We've got a fragment shader, ending this shader pass.
		shaderParts.append(elem)
		shaderPasses.append(ShaderPass(shaderParts))

		shaderParts = []

	if not shaderPasses:
		raise ShaderReaderException("No shaders found in the shader file.")

	return shaderPasses


if __name__ == "__main__":
	import sys
	from pprint import pprint
	with open(sys.argv[1], "r") as handle:
		data = handle.read()

	pprint(parse_shader(data))
