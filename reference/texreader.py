#!/usr/bin/python
import re
import string
from OpenGL.GL import *

TOKEN_RE = re.compile("[^" + string.whitespace + "]+")
WS_RE = re.compile("[" + string.whitespace + "]+")


class TexReaderException(Exception):
	pass


def next_pot(number):
	"""
	Return the first power-of-two greater than a given number.

	Assumes numbers will fit in 32-bit integers; most graphics cards have
	maximum texture sizes vastly smaller than 2**32, so this should be fine.
	"""
	number -= 1
	number |= number >> 1
	number |= number >> 2
	number |= number >> 4
	number |= number >> 8
	number |= number >> 16
	number += 1

	return number


class Texture(object):

	def __init__(self, width, height, pixels=None):
		self.textureID = glGenTextures(1)
		self.imageWidth = width
		self.imageHeight = height
		self.textureWidth = next_pot(width)
		self.textureHeight = next_pot(height)

		glBindTexture(GL_TEXTURE_2D, self.textureID)

		glTexImage2D(
				GL_TEXTURE_2D, # target
				0,             # mipmap level
				GL_RGBA,       # internal format
				self.textureWidth, self.textureHeight,
				0,             # border size
				GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV,
				None,
			)

		if pixels:
			glTexSubImage2D(
					GL_TEXTURE_2D, # target
					0,             # mipmap level
					0, 0,          # (xoffset, yoffset)
					self.imageWidth, self.imageHeight,
					GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV,
					pixels,
				)

		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER)
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER)

	def destroy(self):
		if self.textureID != None:
			glDeleteTextures([self.textureID])
			self.textureID = None
			self.imageWidth = None
			self.imageHeight = None
			self.textureWidth = None
			self.textureHeight = None

	def __del__(self):
		self.destroy()


def parse_ppm(data):
	"""
	Parse the PPM data from the given byte string.
	"""
	try:
		magic, widthStr, heightStr, sampleMaxStr, pixelData = \
				data.split(None, 4)
	except ValueError:
		raise TexReaderException("Can't parse data: %r..." % (data[:40],))

	if magic != "P6":
		raise TexReaderException("Unrecognised magic %r" % (magic,))

	try:
		width = int(widthStr)
		height = int(heightStr)
		sampleMax = int(sampleMaxStr)
	except ValueError, e:
		raise TexReaderException("Can't parse data: %s" % (str(e),))

	if sampleMax != 255:
		raise TexReaderException("Textures must have 8 bits per channel.")

	if len(pixelData) != (width * height * 3):
		raise TexReaderException("Got %d bytes of pixel data, expected %d"
				% (len(pixelData), width * height * 3))

	# Now to convert this packed RGB pixel data to RGBA data that OpenGL can
	# understand.
	pixels = []
	for pxOffset in range(0, (width * height * 3), 3):
		pixels.append(pixelData[pxOffset:pxOffset+3])
		pixels.append("\xff")

	return (width, height, "".join(pixels))


def textureFromFile(filename):
	with open(filename, "rb") as handle:
		width, height, pixels = parse_ppm(handle.read())

	return Texture(width, height, pixels)


if __name__ == "__main__":
	import sys
	with open(sys.argv[1]) as handle:
		data = handle.read()
		print parse_ppm(data)
