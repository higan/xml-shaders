<?xml version="1.0" encoding="UTF-8"?>
<!--
    XML Shader Spec v1.1 Test Shader by Screwtape

    This shader is designed to exercise two corner-cases of the XML Shader Spec
    v1.1, to give implementors a quick visual way to check their
    implementations.

    - This shader draws a radial gradient centred on the (1,1) texture
      coordinate, in each of the R, G, B channels, each in a subsequent shader
      pass.
      - If the host application sets up the orientation of each intermediate
        texture correctly, the final rendered result should show the original
        texture in the correct orientation, with a white radial gradient
        centred on the lower-right corner.
      - If the host application messes something up, the result will probably
        have different-coloured gradients in different corners.

    - This shader temporarily stores red-channel data in the "A" channel of the
      intermediate textures.
      - If the host application allocates intermediate textures with the
        "GL_RGBA" internal format, everything should be fine.
      - If the host application allocates intermediate textures with some other
        format, certain colour channels will probably be forced to 0 or the
        maximum value, adding a weird tint to the input texture.

    - This shader has a pass that overlays an image sampled from outside the
      0..1 texture coordinate range.
      - If the host application correctly sets the texture-clamping mode to
        GL_CLAMP_TO_BORDER, all the colours read from the sampler should be
        pure black, and this pass will have no visible effect.
      - If the host application sets any other kind of texture-clamping mode,
        the colours sampled will be from elsewhere in the texture, and the
        result will be some kind of horrible mess.

    Copyright 2012 Screwtape <screwtape@froup.com>

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
    SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
    IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

 -->
<shader language="GLSL">
    <fragment><![CDATA[
    uniform vec2 rubyInputSize;
    uniform vec2 rubyTextureSize;
    uniform sampler2D rubyTexture;
    void main() {
        vec2 imageCoordMax = rubyInputSize / rubyTextureSize;
        vec2 imageCoord = gl_TexCoord[0].xy / imageCoordMax;
        gl_FragColor = texture2D(rubyTexture, gl_TexCoord[0].xy);
        gl_FragColor.a = gl_FragColor.r + imageCoord.x * imageCoord.y;
    }
    ]]></fragment>
    <fragment><![CDATA[
    uniform vec2 rubyInputSize;
    uniform vec2 rubyTextureSize;
    uniform sampler2D rubyTexture;
    void main() {
        vec2 imageCoordMax = rubyInputSize / rubyTextureSize;
        vec2 imageCoord = gl_TexCoord[0].xy / imageCoordMax;
        gl_FragColor = texture2D(rubyTexture, gl_TexCoord[0].xy);
        gl_FragColor.g += imageCoord.x * imageCoord.y;
        gl_FragColor.r = gl_FragColor.a;
    }
    ]]></fragment>
    <fragment><![CDATA[
    uniform vec2 rubyInputSize;
    uniform vec2 rubyTextureSize;
    uniform sampler2D rubyTexture;
    void main() {
        vec2 imageCoordMax = rubyInputSize / rubyTextureSize;
        vec2 imageCoord = gl_TexCoord[0].xy / imageCoordMax;
        gl_FragColor = texture2D(rubyTexture, gl_TexCoord[0].xy);
        gl_FragColor.b += imageCoord.x * imageCoord.y;
    }
    ]]></fragment>
    <fragment><![CDATA[
    uniform sampler2D rubyTexture;
    void main() {
        gl_FragColor = texture2D(rubyTexture, gl_TexCoord[0].xy);
        gl_FragColor += texture2D(rubyTexture, -gl_TexCoord[0].xy);
    }
    ]]></fragment>
</shader>
